'use strict';
require('dotenv').config({ path: './dev.env' });
const ProductService = require('../services/productService.js');

exports.handler = async (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    let offset, limit;
    if(event && event.queryStringParameters && event.queryStringParameters.offset && event.queryStringParameters.limit){
      offset = event.queryStringParameters.offset;
      limit = event.queryStringParameters.limit;
    }
    const data = await ProductService.getProduct(offset, limit);
    return {
      statusCode: 200,
      body: JSON.stringify(
        {
          message: data,
          input: event,
        }
      ),
    };
};