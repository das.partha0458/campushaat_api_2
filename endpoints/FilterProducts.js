'use strict';
require('dotenv').config({ path: './dev.env' });
const ProductService = require('../services/productService.js');

exports.handler = async(event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    let productName, productDesc, minPrice, maxPrice, category, vendor;
    if(event && event.queryStringParameters){
      productName = !!event.queryStringParameters.productName ? event.queryStringParameters.productName : "";
      productDesc = !!event.queryStringParameters.productDesc ? event.queryStringParameters.productDesc: "";
      minPrice = !!event.queryStringParameters.minPrice ? event.queryStringParameters.minPrice : 0;
      maxPrice = !!event.queryStringParameters.maxPrice ? event.queryStringParameters.maxPrice : 0;
      category = !!event.queryStringParameters.category ? event.queryStringParameters.category : "";
      vendor = !!event.queryStringParameters.vendor ? event.queryStringParameters.vendor : "";
    }
    const data = await ProductService.filterProduct(productName,productDesc,minPrice,maxPrice,category,vendor);
    return {
      statusCode: 200,
      body: JSON.stringify(
        {
          message: data,
          input: event,
        }
      ),
    };
}
  