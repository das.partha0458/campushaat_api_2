const Busboy = require('busboy');

exports.parser = (event, fileZise) =>
    new Promise((resolve, reject) => {
        const busboy = Busboy({
            headers: {
                'content-type':
                event.headers['content-type'] || event.headers['Content-Type']
            },
            limits: {
                fileZise
            }
        });

        const result = {
            files: []
        };

        busboy.on('file', (fieldname, file, info) => {
            const { filename, encoding, mimeType } = info;
            console.log(info);
            const uploadFile = {}
            file.on('data', data => {
                uploadFile.content = data
            });
            file.on('end', () => {
                if (uploadFile.content) {
                    uploadFile.filename = filename
                    uploadFile.contentType = mimeType
                    uploadFile.encoding = encoding
                    uploadFile.fieldname = fieldname
                    result.files.push(uploadFile)
                }
            })
        })

        busboy.on('field', (fieldname, value) => {
            result[fieldname] = value
        });

        busboy.on('error', error => {
            reject(error)
        })

        busboy.on('finish', () => {
            resolve(result);
        })

        busboy.write(event.body, event.isBase64Encoded ? 'base64' : 'binary')
        busboy.end()
 })