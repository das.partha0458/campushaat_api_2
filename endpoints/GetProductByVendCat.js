'use strict';
require('dotenv').config({ path: './dev.env' });
const ProductService = require('../services/productService.js');

exports.handler = async (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    let offset, limit, vendor, category;
    if(event && event.queryStringParameters){
      vendor = !!event.queryStringParameters.vendor ? event.queryStringParameters.vendor : "";
      category = !!event.queryStringParameters.category ? event.queryStringParameters.category : "";
      offset = !!event.queryStringParameters.offset ? event.queryStringParameters.offset : 0;
      limit = !!event.queryStringParameters.limit ? event.queryStringParameters.limit : 10;
    }
    const data = await ProductService.getProductByVendorOrCategory(vendor, category, offset, limit);
    return {
      statusCode: 200,
      body: JSON.stringify(
        {
          message: data,
          input: event,
        }
      ),
    };
};