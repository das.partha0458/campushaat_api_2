const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
let isConnected;
 
module.exports = connectToDatabase = () => {
  if (isConnected) {
    console.log("Connection successfull");
    return Promise.resolve();
  }
  const options = {
    autoIndex: false, // Don't build indexes
    maxPoolSize: 20, // Maintain up to 10 socket connections
    serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
  };
  return mongoose.connect(process.env.DB, options)
    .then(db => { 
        console.log("Connection successfull");
        isConnected = db.connections[0].readyState;
        return true;
    });
};