const mongoose = require('mongoose');

const CampusSettingSchema = new mongoose.Schema({
    campusId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'campus' },
    active: { type: Boolean, required: true },
    startDate: { type: Date, required: true, default: Date.now },
    endDate: { type: Date, required: true, default: Date.now }
});

const VendorSchema = new mongoose.Schema({
    vendorBusinessName: { type: String, required: true },
    vendorName: { type: String, required: true },
    vendorInfo: { type: String, required: true },
    vendorEmail: { type: String, required: true },
    vendorContact: { type: String, required: true },
    vendorSocial: { type: [String], required: true },
    vendorAddress: { type: String, required: true },
    vendorMedia: { type: [String], required: true },
    campusSetting: { type: [CampusSettingSchema], required: true },
    openStatus: { type: Boolean, required: true, default: true },
    createdDate:{ type: Date, required: true, default: new Date().toISOString()},
    modifiedDate: { type: Date, required: true, default: new Date().toISOString()}
});

module.exports = mongoose.models.Vendor || mongoose.model('Vendor', VendorSchema);


