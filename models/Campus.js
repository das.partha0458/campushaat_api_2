const mongoose = require('mongoose');

const CampusSchema  = new mongoose.Schema({
    campusName: { type: String, required: true },
    campusInfo: { type: String, required: true },
    imageUrl: { type: String, required: true },
    campusType: { type: String, required: false },
    openTime: { type: Date, required: true, default: new Date().toISOString()},
    closeTime: { type: Date, required: true, default: new Date().toISOString()},
    createdDate: { type: Date, required: true, default: new Date().toISOString()},
    modifiedDate: { type: Date, required: true, default: new Date().toISOString()}
});

module.exports = mongoose.models.Campus || mongoose.model('Campus', CampusSchema);