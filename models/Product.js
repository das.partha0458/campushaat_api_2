const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({  
    productName: { type: String, required: true },
    productDesc: { type: String, required: true },
    productCategory: { type: String, required: true },
    productType: { type: String, required: true },
    kitchenName: { type: String, required: true },
    quantityInStock: { type: Number, required: true },
    quantityRemainStock: { type: Number, required: true },
    productMedia: { type: [String], required: true },
    productPrepTime: { type: String, required: true },
    productAnalytics: { type: mongoose.Schema.Types.ObjectId, required: true },
    productSoldBy: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'vendor' },
    productCreatedBy: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'vendor' },
    productPrice: { type: Number, required: true },
    productOriginalPrice: { type: Number, required: true },
    productDiscount: { type: Number, required: true, default: 0 },
    currency: { type: String, required: true, default: 'INR' },
    isTax: { type: Boolean, required: true },
    taxAmount: { type: Number, required: true, default: 0 },
    productOriginalPriceWithTax: { type: Number, required: true },
    productUnit: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'unit' },
    createdDate:{ type: Date, required: true, default: Date.now },
    modifiedDate: { type: Date, required: true, default: Date.now }
});

module.exports = mongoose.models.Product || mongoose.model('Product', ProductSchema);
    