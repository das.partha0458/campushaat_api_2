require('dotenv').config({ path: './dev.env' });
const connectToDatabase = require('../DB.js');
const Product = require('../models/Product.js');
const Vendor = require('../models/Vendor.js');

const getProduct = async (offset, limit) => {
  const connection = await connectToDatabase();
  return await Product.find({},{
    _id: 0,
    productName: 1,
    productDesc: 1,
    productCategory: 1,
    kitchenName: 1,
    productMedia: 1,
    productPrice: 1,
    productOriginalPrice: 1,
    productDiscount: 1
  })
  .skip(offset).limit(limit)
  .populate({path: 'productSoldBy', model: Vendor})
  .exec();    
}

const getProductByVendorOrCategory = async (vendor, category, offset, limit) => {
  const connection = await connectToDatabase();
  if(vendor !== ""){
    return await Product.find({kitchenName: vendor},{
      _id: 0,
      productName: 1,
      productDesc: 1,
      productCategory: 1,
      kitchenName: 1,
      productMedia: 1,
      productPrice: 1,
      productOriginalPrice: 1,
      productDiscount: 1
    })
    .skip(offset).limit(limit)
    .populate({path: 'productSoldBy', model: Vendor})
    .exec(); 
  }else if(category !== ""){
    return await Product.find({productCategory: category},{
      _id: 0,
      productName: 1,
      productDesc: 1,
      productCategory: 1,
      kitchenName: 1,
      productMedia: 1,
      productPrice: 1,
      productOriginalPrice: 1,
      productDiscount: 1
    })
    .skip(offset).limit(limit)
    .populate({path: 'productSoldBy', model: Vendor})
    .exec(); 
  }
}

const filterProduct = async (productName, productDesc, minPrice, maxPrice, category, vendor) => {
  const connection = await connectToDatabase();
  let query = [
    {
      '$match': {
        'productCategory': {
          '$eq': category
        }, 
        'productName': {
          '$regex': '.*'+ productName +'.*'
        }, 
        'productDesc': {
          '$regex': '.*'+ productDesc +'.*'
        }, 
        'kitchenName': {
          '$regex': '.*'+ vendor +'.*'
        }
      }
    }, {
      '$match': {
        'productPrice': {
          '$gt': Number(minPrice), 
          '$lt': Number(maxPrice)
        }
      }
    }, {
      '$lookup': {
        'from': 'units', 
        'localField': 'productUnit', 
        'foreignField': '_id', 
        'as': 'unit'
      }
    }
  ];
  console.log(JSON.stringify(query));
  return await Product.aggregate(query).exec();    
}

module.exports = {
    getProduct,
    getProductByVendorOrCategory,
    filterProduct
}